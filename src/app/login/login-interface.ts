export interface ILoginInterface {
  'Emp_Id': string;
  'Emp_Email': string;
  'Emp_Name': string;
  'Emp_Role': string;
  'Emp_Phone': string;
  'location': string;
}
