import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { LoginService } from './login.service';
import { SharedServiceService } from '../shared/shared-service.service';


@Component ({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    error : boolean = false;
    errorMessage : string = "";
    private userId: string;
    private userPassword: string;

     constructor(
      public router: Router,
      private loginService: LoginService,
      private sharedservice: SharedServiceService
    ) {}

    ngOnInit() {}

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    private authenticateUser(formData) {

      this.userId = formData.inputUsername;
      this.userPassword = formData.inputPassword;
      const postData = {
          'userId' : this.userId,
          'userPassword' :  this.userPassword
      }

      this.loginService.getUserAuthentication(postData).subscribe(
        (data) => {
          console.log('Response received');
          console.log(data);
          if(data["errorCode"] === "0"){
            
            this.sharedservice.setloginData(data.userData);
            // window.localStorage.setItem('userData',data['userData'].token);
            this.router.navigate(['/dashboard']);  //navigate dashboard page it authentication is successfull.
          }else{
            this.error = true;
            this.errorMessage= data["errorMessage"];
          }
        },
        (error) => {
          console.log('Error while authenticating user' + error);
        },
        () => {
          console.log('authentication completely');
        }
      );

    }

}
