import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CommonConstant } from 'src/Common/common.constants';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginUrl = 'login/authenticate';
 
  constructor(private http: HttpClient) { }

  public getUserAuthentication(recievedData): Observable<any> {
    const url = CommonConstant.localErpApiUrl + this.loginUrl;
    const postData = {
      "userid" : recievedData.userId,
      "password" : recievedData.userPassword
    };
    return this.http.post<any>(url, postData).pipe(map(data => data));
  }

}
