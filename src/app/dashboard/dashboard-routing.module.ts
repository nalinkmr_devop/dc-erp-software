import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        children: [
            { path: '', redirectTo: 'quilifiedlead', pathMatch: 'prefix' },
            { path: 'quilifiedlead', loadChildren: './lead-management/quilifiedlead/quilifiedlead.module#QuilifiedleadModule' },
            { path: 'mymeeting' , loadChildren: './mymeeting/mymeeting.module#MymeetingModule'},
            { path: 'availability-management' , loadChildren: './availability-management/availability-management.module#AvailabilityManagementModule'},
            { path: 'schedulemeeting', loadChildren: '../component-sibling/schedule-meeting/schedule-meeting.module#ScheduleMeetingModule' },
            { path: 'followupmeeting', loadChildren: '../component-sibling/followup-meeting/followup-meeting.module#FollowupMeetingModule' },
            { path: 'closed', loadChildren: '../component-sibling/closed/closed.module#ClosedModule' },
            { path: 'dropped', loadChildren: '../component-sibling/dropped/dropped.module#DroppedModule' },
            { path: 'leadmeeting', loadChildren: './availability-management/lead-meeting/lead-meeting.module#LeadMeetingModule' },
            { path: 'project', loadChildren: './availability-management/project/project.module#ProjectModule' }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {}
