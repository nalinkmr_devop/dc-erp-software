import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeadManagementComponent } from './lead-management.component';


const routes: Routes = [
    {
        path: '',
        component: LeadManagementComponent,
        children: [
          { path: 'quilifiedlead', loadChildren: './quilifiedlead/quilifiedlead.module#QuilifiedleadModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LeadManagementRoutingModule {}
