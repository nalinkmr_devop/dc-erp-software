import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadManagementComponent } from './lead-management.component';

@NgModule({
  declarations: [LeadManagementComponent],
  imports: [
    CommonModule
  ]
})
export class LeadManagementModule { }
