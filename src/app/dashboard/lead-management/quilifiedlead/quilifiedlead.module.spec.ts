import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { QuilifiedleadComponent } from './quilifiedlead.component';
import { QuilifiedleadModule } from './quilifiedlead.module';

describe('QuilifiedleadComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ QuilifiedleadModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(QuilifiedleadComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
