import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuilifiedleadRoutingModule } from './quilifiedlead-routing.module';
import { QuilifiedleadComponent } from './quilifiedlead.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, QuilifiedleadRoutingModule,FormsModule],
    declarations: [QuilifiedleadComponent]
})
export class QuilifiedleadModule {}
