import { Component, OnInit } from '@angular/core';
import { SharedServiceService } from 'src/app/shared/shared-service.service';
import { QuilifiedleadService } from './quilifiedlead.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-quilifiedlead',
  templateUrl: './quilifiedlead.component.html',
  styleUrls: ['./quilifiedlead.component.scss']
})
export class QuilifiedleadComponent implements OnInit {

  public userData: any;
  error: boolean;
  errorMessage: any;
  location : any;
  public storedLeadData: any;
  enqueryId: any;
  customerName: any;
  emailId: any;
  mobile: any;

  constructor(private sharedservice: SharedServiceService, private quilifiedleadservice: QuilifiedleadService, private router: Router) { }

  ngOnInit() {
    this.sharedservice.getLoginData().subscribe(data => this.userData = data);
    const postData = {
      // 'Emp_Role' : this.userData.Emp_Role, //when emp_role will be needed.
      'location' : this.userData.location
    }

    this.quilifiedleadservice.getLeadByLocation(postData).subscribe(
      (data) => {

        if (data.errorcode === "0") {
          var   searchText;
          this.storedLeadData = data.leadData;
          console.log(this.storedLeadData);
        } else {
          this.error = true;
          this.errorMessage = data["errorMessage"];
        }
      },
      (error) => {
        console.log('Error while geting lead' + error);
      },
      () => {
        console.log('Fectching lead completely');
      }
    );
  }

  // to display user data on the modal
  openModal(get){
    this.enqueryId = get.enqueryId;
    this.customerName =get.customerName;
    this.emailId =get.emailId;
    this.mobile =get.mobile;
    this.location = get.region;
  }
  
}
