import { TestBed } from '@angular/core/testing';

import { QuilifiedleadService } from './quilifiedlead.service';

describe('QuilifiedleadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuilifiedleadService = TestBed.get(QuilifiedleadService);
    expect(service).toBeTruthy();
  });
});
