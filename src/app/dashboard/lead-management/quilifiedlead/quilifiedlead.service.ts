import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonConstant } from 'src/Common/common.constants';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuilifiedleadService {

  private leadUrl =  'lead/getleadbylocation';

  constructor(private http : HttpClient) { }

  public getLeadByLocation(getData): Observable<any> {
    const url = CommonConstant.localErpApiUrl + this.leadUrl;
    const postData = {
      // "emp_role" : recievedData.userId,
      "location" : getData.location
    };
    return this.http.post<any>(url, postData).pipe(map(data => data));
  }

}
