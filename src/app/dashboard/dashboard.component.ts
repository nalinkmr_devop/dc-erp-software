import { Component, OnInit } from '@angular/core';
import { SharedServiceService } from '../shared/shared-service.service';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    public loginData: any;
    collapedSideBar: boolean;

    constructor(private sharedservice: SharedServiceService) {}

    ngOnInit() {
      this.sharedservice.getLoginData().subscribe( data => this.loginData = data);
      console.log(this.loginData);
    }

    receiveCollapsed($event) {
        this.collapedSideBar = $event;
    }
}
