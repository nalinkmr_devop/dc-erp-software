import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AvailabilityManagementComponent } from './availability-management.component'

const routes: Routes = [
    {
        path: '', component: AvailabilityManagementComponent,
        children: [
            { path: 'dashboard/leadmeeting', redirectTo: 'leadmeeting', pathMatch: 'prefix' },
            { path: 'lead-meeting', loadChildren: './lead-meeting/lead-meeting.module#LeadMeetingModule' },
            { path: 'project', loadChildren: './project/project.module#ProjectModule' }
          ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AvailabilityManagementRoutingModule {
}
