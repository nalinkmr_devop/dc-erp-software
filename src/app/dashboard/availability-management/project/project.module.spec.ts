import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProjectModule } from './project.module';
import { ProjectComponent } from './project.component';


describe('ProjectComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ProjectModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(ProjectComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
