import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvailabilityManagementRoutingModule } from './availability-management-routing.module';
import { AvailabilityManagementComponent } from './availability-management.component';

@NgModule({
    imports: [CommonModule, AvailabilityManagementRoutingModule],
    declarations: [AvailabilityManagementComponent]
})
export class AvailabilityManagementModule {}
