import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AvailabilityManagementComponent } from './availability-management.component';
import { AvailabilityManagementModule } from './availability-management.module';

describe('AvailabilityManagementComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ AvailabilityManagementModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(AvailabilityManagementComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
