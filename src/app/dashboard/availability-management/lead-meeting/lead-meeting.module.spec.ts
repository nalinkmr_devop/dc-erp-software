import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LeadMeetingModule } from './lead-meeting.module';
import { LeadMeetingComponent } from './lead-meeting.component';

describe('LeadMeetingComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ LeadMeetingModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(LeadMeetingComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
