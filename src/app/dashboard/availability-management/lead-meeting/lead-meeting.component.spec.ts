import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadMeetingComponent } from './lead-meeting.component';

describe('LeadMeetingComponent', () => {
  let component: LeadMeetingComponent;
  let fixture: ComponentFixture<LeadMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
