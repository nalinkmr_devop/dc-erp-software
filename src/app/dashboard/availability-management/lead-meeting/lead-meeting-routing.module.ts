import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeadMeetingComponent } from './lead-meeting.component';



const routes: Routes = [
    {
        path: '', component: LeadMeetingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LeadMeetingRoutingModule {
}
