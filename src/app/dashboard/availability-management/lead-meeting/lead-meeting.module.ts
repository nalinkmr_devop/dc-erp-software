import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadMeetingComponent } from './lead-meeting.component';
import { LeadMeetingRoutingModule } from './lead-meeting-routing.module';

@NgModule({
    imports: [CommonModule, LeadMeetingRoutingModule],
    exports:[],
    declarations: [LeadMeetingComponent]
})
export class LeadMeetingModule {}
