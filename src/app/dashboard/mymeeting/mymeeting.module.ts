import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MymeetingRoutingModule } from './mymeeting-routing.module';
import { MymeetingComponent } from './mymeeting.component';

@NgModule({
    imports: [CommonModule, MymeetingRoutingModule],
    declarations: [MymeetingComponent]
})
export class MymeetingModule {}
