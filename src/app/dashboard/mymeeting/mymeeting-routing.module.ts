import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MymeetingComponent } from './mymeeting.component';

const routes: Routes = [
    {
        path: '', component: MymeetingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MymeetingRoutingModule {
}
