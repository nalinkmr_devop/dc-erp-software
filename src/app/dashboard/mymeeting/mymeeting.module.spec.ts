import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MymeetingComponent } from './mymeeting.component';
import { MymeetingModule } from './mymeeting.module';

describe('MymeetingComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MymeetingModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(MymeetingComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
