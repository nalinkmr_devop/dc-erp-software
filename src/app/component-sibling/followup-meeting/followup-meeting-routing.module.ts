import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FollowupMeetingComponent } from './followup-meeting.component';



const routes: Routes = [
    {
        path: '', component: FollowupMeetingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FollowupMeetingRoutingModule {
}
