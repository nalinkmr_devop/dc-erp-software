import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FollowupMeetingRoutingModule } from './followup-meeting-routing.module';
import { FollowupMeetingComponent } from './followup-meeting.component';



@NgModule({
    imports: [CommonModule, FollowupMeetingRoutingModule],
    exports:[],
    declarations: [FollowupMeetingComponent]
})
export class FollowupMeetingModule {}
