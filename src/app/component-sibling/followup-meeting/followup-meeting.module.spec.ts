import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';


import { FollowupMeetingComponent } from './followup-meeting.component';
import { FollowupMeetingModule } from './followup-meeting.module';


describe('FollowupMeetingComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FollowupMeetingModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(FollowupMeetingComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
