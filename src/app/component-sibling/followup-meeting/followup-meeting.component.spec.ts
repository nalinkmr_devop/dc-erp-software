import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowupMeetingComponent } from './followup-meeting.component';

describe('FollowupMeetingComponent', () => {
  let component: FollowupMeetingComponent;
  let fixture: ComponentFixture<FollowupMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowupMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowupMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
