import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentSiblingComponent } from './component-sibling.component';

describe('ComponentSiblingComponent', () => {
  let component: ComponentSiblingComponent;
  let fixture: ComponentFixture<ComponentSiblingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentSiblingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentSiblingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
