import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-sibling',
  templateUrl: './component-sibling.component.html',
  styleUrls: ['./component-sibling.component.scss']
})
export class ComponentSiblingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
