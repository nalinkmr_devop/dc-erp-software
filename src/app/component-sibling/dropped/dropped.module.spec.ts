import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DroppedModule } from './dropped.module';
import { DroppedComponent } from './dropped.component';


describe('DroppedComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ DroppedModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(DroppedComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
