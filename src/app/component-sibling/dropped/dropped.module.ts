import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DroppedRoutingModule } from './dropped-routing.module';
import { DroppedComponent } from './dropped.component';

@NgModule({
    imports: [CommonModule, DroppedRoutingModule],
    exports:[],
    declarations: [DroppedComponent]
})
export class DroppedModule {}
