import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DroppedComponent } from './dropped.component';

const routes: Routes = [
    {
        path: '', component: DroppedComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DroppedRoutingModule {
}
