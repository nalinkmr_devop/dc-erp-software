import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleMeetingRoutingModule } from './schedule-meeting-routing.module';
import { ScheduleMeetingComponent } from './schedule-meeting.component';

@NgModule({
    imports: [CommonModule, ScheduleMeetingRoutingModule],
    exports:[],
    declarations: [ScheduleMeetingComponent]
})
export class ScheduleMeetingModule {}
