import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ScheduleMeetingComponent } from './schedule-meeting.component';
import { ScheduleMeetingModule } from './schedule-meeting.module';


describe('ScheduleMeetingComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ScheduleMeetingModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(ScheduleMeetingComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
