import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ClosedModule } from './closed.module';
import { ClosedComponent } from './closed.component';

describe('ClosedComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ClosedModule, RouterTestingModule ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(ClosedComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
