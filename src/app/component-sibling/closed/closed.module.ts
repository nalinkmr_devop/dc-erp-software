import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClosedRoutingModule } from './closed-routing.module';
import { ClosedComponent } from './closed.component';

@NgModule({
    imports: [CommonModule, ClosedRoutingModule],
    exports:[],
    declarations: [ClosedComponent]
})
export class ClosedModule {}
