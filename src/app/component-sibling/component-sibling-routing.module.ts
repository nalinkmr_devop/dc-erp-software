import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentSiblingComponent } from './component-sibling.component';

const routes: Routes = [
    {
        path: '',
        component: ComponentSiblingComponent,
        children: [
        { path: 'schedule-meeting' , loadChildren: './schedule-meeting/schedule-meeting.module#ScheduleMeetingModule'},
        { path: 'followup-meeting' , loadChildren: './followup-meeting/followup-meeting.module#FollowupMeetingModule'},
        { path: 'dropped' , loadChildren: './dropped/dropped.module#DroppedModule'},
        { path: 'closed' , loadChildren: './closed/closed.module#ClosedModule'}

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ComponentSiblingRoutingModule {}
