import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ILoginInterface } from '../login/login-interface';

@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {

  constructor() { }
  data = {

  };
  public subject = new BehaviorSubject(this.data);

  // Getter
  public getLoginData() {
    return this.subject;
  }

  // Setter
  public setloginData(data: ILoginInterface) {
    this.subject.next(data);
  }

}
