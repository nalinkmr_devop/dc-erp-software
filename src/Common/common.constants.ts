export class CommonConstant {
  public static readonly proxyUrl: string = 'https://cors-anywhere.herokuapp.com/';
  public static readonly  localErpApiUrl: string = 'http://localhost:8080/dc_erp/dcservice/';
  public static readonly prodErpApiUrl: string = '';
}
